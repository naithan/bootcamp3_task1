//
//  AlphaQuestViewController.m
//  UIViews_bootcamp
//
//  Created by Marcel Starczyk on 12/04/15.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import "AlphaQuestViewController.h"

@interface AlphaQuestViewController ()

@end

@implementation AlphaQuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alphaCheck {
    if (self.myRectangle.alpha < 1.0f) {
        [UIView animateWithDuration:0.75f animations:^{
            self.proceedButton.alpha = 1.0f;
        }];
    }
}

- (IBAction)fadeMe:(id)sender {
    [self performSelector:@selector(alphaCheck) withObject:self afterDelay:1.0f];
    /**
     * * * 4 * * *
     WRITE YOUR CODE HERE
     * * * * * * *
     
     Your task is to animate change in transparency of our little rectangle friend.
     Make him disappear.
     */
    [UIView animateWithDuration:1 animations:^{
        [self.myRectangle setAlpha:0];
        
    }];
    
}

-(void)dealloc {
}

@end
