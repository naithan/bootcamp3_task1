//
//  HierarchyQuestViewController.h
//  UIViews_bootcamp
//
//  Created by Marcel Starczyk on 12/04/15.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HierarchyQuestViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *myRectangle;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;

@end
