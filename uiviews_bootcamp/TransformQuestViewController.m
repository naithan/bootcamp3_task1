//
//  TransformQuestViewController.m
//  UIViews_bootcamp
//
//  Created by Marcel Starczyk on 12/04/15.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import "TransformQuestViewController.h"

@interface TransformQuestViewController ()

@end

@implementation TransformQuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UIView *newObject = (UIView *)object;
    if (NSStringFromCGAffineTransform(newObject.transform)) {
        [UIView animateWithDuration:0.75f animations:^{
            self.proceedButton.alpha = 1.0f;
        }];
    }
}

- (IBAction)transoformMe:(id)sender {
    [self.myRectangle addObserver:self forKeyPath:@"transform" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:NULL];
    /**
     * * * 3 * * *
     WRITE YOUR CODE HERE
     * * * * * * *
     
     Your task is to animate our rectangle friend using transform property. You can scale,
     rotate or translate him - the choice is yours.
     
     You can choose one of three: 
     
     Scale - 1.5 factor
     Rotation - 360 degrees
     Translation - 100 points on x and y axis
     
     Bonus points for executing two transforms simultaneously - scale by factor of 1.5 and rotation by 360 degrees.
     */
    CGPoint center = self.myRectangle.center;
    CGFloat transform = 100;
    
    [UIView animateWithDuration:1 animations:^{
        [self.myRectangle setTransform:(CGAffineTransformMakeTranslation(transform, transform))];
        [self.myRectangle setTransform:(CGAffineTransformMakeScale(1.5, 1.5))];
        
    }];
    
}

-(void)dealloc {
    [self.myRectangle removeObserver:self forKeyPath:@"transform"];
}

@end
