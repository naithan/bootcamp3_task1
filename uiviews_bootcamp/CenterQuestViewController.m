//
//  CenterQuestViewController.m
//  UIViews_bootcamp
//
//  Created by Marcel Starczyk on 12/04/15.
//  Copyright (c) 2015 Droids on Roids. All rights reserved.
//

#import "CenterQuestViewController.h"

@interface CenterQuestViewController ()

@property (assign,nonatomic) CGPoint myRectangleOrigin;

@end

@implementation CenterQuestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
- (void)viewDidAppear:(BOOL)animated {
    
    self.myRectangleOrigin = self.myRectangle.frame.origin;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if(self.myRectangleOrigin.x != self.myRectangle.frame.origin.x ||
       self.myRectangleOrigin.y != self.myRectangle.frame.origin.y ) {
        [UIView animateWithDuration:0.75f animations:^{
            self.proceedButton.alpha = 1.0f;
        }];
    }
    
}

- (IBAction)moveMe:(id)sender {
    [self.myRectangle addObserver:self forKeyPath:@"center" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:NULL];
    /**
     * * * 2 * * *
     WRITE YOUR CODE HERE
     * * * * * * *
     
     Your task is to animate change in position of our rectangle friend.
     
     Move him 100 points down.
     
     */
    CGPoint center = self.myRectangle.center;
    center.y = center.y + 100;
    
    [UIView animateWithDuration:1 animations:^{
        [self.myRectangle setCenter:center];
    }];
}

-(void)dealloc {
    [self.myRectangle removeObserver:self forKeyPath:@"center"];
}

@end
